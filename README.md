# 基礎セミナー2018: 自動運転 で使う通信用ライブラリ(?)

## 重要なファイル

(6/22現在)

主にこれらのファイルを使用

* `socketserver.py`: クライアントからの接続を受けてメッセージをストア・配信します
* `communicator.py`: 通信周りを処理するための関数およびデコレータを提供します
* `jupyter_poll.py`: Jupyter notebookのウィジェット機能を利用して新規メッセージの有無を確認します

内部的 or 補助的に使用

* `helper.py`: `api_set.json`をパースし，他のクラスで利用できるようなしくみを提供します．また，サーバ・クライアント側で(関数/メソッドとして)機能が実装されているかのチェッカも提供します
* `api_set.json`: サーバサイド・クライアントサイドで実装されるべき機能と，メッセージ形式を定義します
* `sup_obj`(directory): 内部のファイルはすべて`socketserver.py`から分離されたものです
* `web`(directory): 登録済みの人の情報を表示します．

## 使い方

### socketserver.py

実行するとワーカスレッドを生成し，入ってきた接続を取り合う形で処理を行います．内部にクライアントプールを持っており，入ってきたメッセージや状態は送信者の名前ベースでストアされます．  
また，10秒おきに標準出力に接続があった各クライアントの状態を出力します．接続・切断，呼び出された機能などのログについては標準出力・ファイルまたはその両方に出力できます．

基本は`python socketserver.py`でOKです．この場合このプログラムは`0.0.0.0:8889`で待機します．  
オプションとして，以下を用意しました．バインドするIPの変更(必要ないはず)や，ポートの変更(これは使うかも)，およびわーカスレッドの数を変更できます．

* `--host`: どのホストとして待ち受けるかを決めます(デフォルト: `0.0.0.0`)．通常，変更の必要はありません．
* `--port`: どのポートで待ち受けるかを決めます(デフォルト: `8889`)．他のプログラムがポートを塞いでいる場合は別のポートを使うようにしてください．
* `--jobs`: ワーカスレッドの数を決めます(デフォルト: 4)．同時接続数が膨大な数になる，CPUを食いつぶされるような場合に変更してください．
* `--log`: このプログラムがログを出力する場所を指定します．`stdout`か`file`を選択でき，`stdout:file`とした場合は両方に出力します．
* `--clear-log`: `--log`に`file`が含まれるときだけ動作します．指定すると，ログファイルを追記ではなく書き込みで開きます．

#### 提供する機能群

* `register_me`: 新規に人を登録します．
* `register_message`: 他者へのメッセージを登録します
* `register_status`: 自己の位置と現在の行動を登録します
* `get_message`: 特定人物の登録したメッセージを取得します
* `get_status`: 特定人物の位置と現在の行動を取得します(使用の必要はないと思います)
* `get_latest_status_of_all`: 全員の最新の位置と情報を取得します

### communicator.py

サーバとメッセージをやりとりするためのクラスとデコレータを提供します．  
(注: デコレータの使用は非推奨です．実験的に書きましたが，現在は動作しません)

このファイルでは，クライアント-サーバ間のメッセージングはサポートしますが，**個々のクライアントがやり取りするメッセージの形式については特に規定しません**  
**(=`send_message`で送る内容は辞書だろうとテキストだろうと，シリアライズできればよい)**

#### CommunicationClient

唯一の，そしてメインになるクラスです．

* `CommunicationClient(myname, category, remote_ip, remote_port)`:  
  コンストラクタです．今後の情報をやり取りするサーバ・ポートを`remote_ip:remote_port`に設定し，`myname`および`category`で以降の通信を行います．  
  例) `CommunicationClient("Alice", "consumer", "0.0.0.0", 8889)`
* `dict __send(message)`: 通常利用する必要はありません．他のメソッドを呼ぶ際に暗黙的に呼ばれます．事実上のprivateメソッドとして機能するため，インスタンスの外からは見えないはずです．
* `dict register_me()`: サーバに自身を登録します．必ず呼ばなければなりません．返り値はサーバからのレスポンスjsonです
* `dict send_position(position)`:  
  `position`をサーバに送ります．`position`は`int`もしくは`float`でなければなりません．返り値はサーバからのレスポンスjsonです
* `dict send_status(status)`: `status`をサーバに送ります．`status`は`str`でなければなりません．返り値はサーバからのレスポンスjsonです．
* `dict send_message(message, to)`: 任意のメッセージ`message`を`to`に送ります．ただし，`json.dumps`で直列化できるものに限ります．また，`to`に`broadcast`を指定すると自分を含む全員にメッセージが送信されます．ただし，登録されていない人を`to`に指定することはできません．返り値はサーバからのレスポンスjsonです
* ` dict get_message(only_new=True, only_main=True, sent_by=[], mark_as_read=True)`:  
  自分宛てのメッセージを取得します．`only_new = True`の場合はサーバ側で配信済みフラグのないものが取得されます．`only_main`は通常`True`にしてください．`False`の場合は，サーバに送られたメッセージがそのままの形で返信になります．`sent_by`は送信者をフィルタするために使います．空のリストの場合は制限しません．`mark_as_read`はサーバ側のみ配信メッセージに配信済みフラグを立てるかどうかです．通常`True`にしてください．

#### jupyter_poll.py

`CoommunicationClient`と協調動作するよう設計したJupyter notebook用の関数を提供します．

* `threading.Event poll_message(name, host, port, interval=2, **kwargs)`:  
  `name`に対して新規メッセージが届いているかを`host:port`に`interval`[秒]ごとに問い合わせ，Jupyterのボタン機能を流用したウィジェットに表示します．`kwargs`は無視されます．  
  この関数は`CommunicationClient`を使うと便利に実行できます．`CommunicationClient`インスタンスを`c`とすると，`jupyter_poll.poll_message(**c)`で各引数が適切にセットされた状態となるはずです．  
  内部的には`threading.Thread`で実装されており，バックグラウンドで動作します．関数の戻り値は`threading.Event`で，`set()`を呼び出すとこのウィジェットの実行は止まります．

## メッセージ形式

すべてのメッセージは`api_set.json`に記載されています．`api_set.json`は`server_side`と`client_side`および`common`の3クラスに分割されており，それぞれサーバ側のみで実装，クライアント側のみで実装，両側で実装を意味します．  
各クラスにおけるキーは関数名に対応し，キーに対応する値はその関数によるメッセージを規定するものです．また，メッセージ内で`null`となっている項目については値をセットしない限りエラーになります．

* クライアント -> サーバ: json形式で以下のものを想定します．したがって，pythonの`json.dumps()`でシリアライズできるオブジェクトでなければなりません．

```json
{
    "control": "必須．サーバの挙動制御に利用する文字列(pool, individual)",
    "action": "必須．サーバ側で呼び出す関数を指定します(register_meなどなど)",
    "message": "メッセージ本体(呼び出す関数に応じ変化)",
    "from": "必須．送信者の名前",
    "to": "呼び出す機能によっては不要"
}
```

* サーバ -> クライアント: json形式で以下のものを想定します．

```json
{
    "result": "処理の成否を示す文字列で， success か fail",
    "message": "処理結果(呼んだ機能次第で変化)"
}
```

------------------

## 使用例

**example.ipynb**にJupyter notebook向けの使い方を記載しています．

---------------------

サーバ側では標準出力に以下のようなメッセージが流れます．

```
           |           |   message received    |          |
      name |  category | before/after delivery | position | status
     Alice |  customer |          0/0          |       -1 | No action
       Bob |      taxi |          0/0          |       -1 | No action
---------- | --------- | --------------------- | -------- | ------

           |           |   message received    |          |
      name |  category | before/after delivery | position | status
     Alice |  customer |          2/0          |       -1 | No action
       Bob |      taxi |          1/0          |       -1 | No action
---------- | --------- | --------------------- | -------- | ------

           |           |   message received    |          |
      name |  category | before/after delivery | position | status
     Alice |  customer |          0/2          |       -1 | No action
       Bob |      taxi |          1/0          |       -1 | No action
---------- | --------- | --------------------- | -------- | ------
```
