#-*-coding:utf-8-*-

import inspect
import json
import socket

import helper

class CommunicationClient:
    remote_ip = None
    remote_port = None
    myname = None
    category = None
    api = None

    def __init__(self, myname, category, remote_ip, remote_port):
        # validation
        if type(myname) is str:
            if "broadcast" in myname or "no-one" in myname:
                raise ValueError("name is containing reserved word")
            else:
                self.myname = myname
        else:
            raise TypeError("myname has invalid type: {0}. Expected: str".format(type(myname)))

        if type(category) is str:
            self.category = category
        else:
            raise TypeError("type has invalid type: {0}. Expected: str".format(type(type)))

        if type(remote_ip) is str:
            self.remote_ip = remote_ip
        else:
            raise TypeError("remote_ip has invalid type: {0}. Expected: str".format(type(remote_ip)))

        if type(remote_port) is int:
            self.remote_port = remote_port
        else:
            raise TypeError("remote_port has invalid type: {0}. Expected: int".format(type(remote_port)))

        self.api = helper.APIHelper("client_side")

    def __send(self, message):
        """
            all other communication function call this at last to send message.
            this funciton sends serialized message.
            one can directly call server-side function using this function (but not recommended)
        """
        # supress ConnectionRefusedError
        try:
            connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            connection.connect((self.remote_ip, self.remote_port))
            connection.send((json.dumps(message) + "\n").encode())

            # may need change
            resp = connection.recv(4096)
            resp = resp.decode()

            connection.close()

            return json.loads(resp)
        except ConnectionRefusedError as e:
            print("Connection failed. Report to the staff")
            print(e)

            # when connection was failed, empty dict will return
            return {}
        except Exception as e:
            print("untrapped error occured. Report to the staff")
            print("message was ... ")
            print(message)
            print("\nerror was ... ")
            print(e)

    def register_me(self):
        message = self.api[inspect.currentframe().f_code.co_name]
        message["from"] = self.myname
        message["message"]["name"] = self.myname
        message["message"]["category"] = self.category
        ret = self.__send(message)

        return ret

    def send_position(self, position):
        # validation before sending
        if type(position) not in [int, float]:
            raise TypeError("position must be int or float")

        message = self.api[inspect.currentframe().f_code.co_name]
        message["from"] = self.myname
        message["message"]["position"] = position
        ret = self.__send(message)

        return ret

    def send_status(self, status):
        # validation before sending
        if type(status) is not str:
            raise TypeError("status must be str")

        message = self.api[inspect.currentframe().f_code.co_name]
        message["from"] = self.myname
        message["message"]["status"] = status
        ret = self.__send(message)

        return ret

    def send_message(self, message, to):
        send_message = self.api[inspect.currentframe().f_code.co_name]
        send_message["from"] = self.myname
        send_message["to"] = to
        send_message["message"] = message
        ret = self.__send(send_message)

        return ret

    def get_message(self, only_new=True, only_main=True, sent_by=[], mark_as_read=True):
        message = self.api[inspect.currentframe().f_code.co_name]
        message["from"] = self.myname
        message["message"]["only_new"] = only_new
        message["message"]["only_main"] = only_main
        message["message"]["sent_by"] = sent_by
        message["message"]["mark_as_read"] = mark_as_read
        ret = self.__send(message)

        return ret

    def keys(self):
        """
            This method is only for unpacking!!
            NEVER think this class as dict-like
        """
        return ["name", "category", "host", "port"]

    def __getitem__(self, key):
        """
            This method is only for unpacking!!
        """
        if key == "name":
            return self.myname
        elif key == "category":
            return self.category
        elif key == "host":
            return self.remote_ip
        elif key == "port":
            return self.remote_port
        else:
            return None


def send_message(message, remote_ip, remote_port):
    connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    connection.connect((remote_ip, remote_port))
    connection.send((json.dumps(message) + "\n").encode())

    # may need change
    resp = connection.recv(4096)
    print(resp.decode())

    connection.close()

def get_debugging_client(register=False):
    """
        this function is written because making clients are troublesome.
    """
    c1 = CommunicationClient("alice", "customer", "0.0.0.0", 8889)
    c2 = CommunicationClient("bob", "taxi", "0.0.0.0", 8889)

    if register:
        c1.register_me()
        c2.register_me()

    return c1, c2

def check_functions():
    c1, c2 = get_debugging_client(register=True)

    print(c1.send_position(50))
    print(c1.send_status("hogehoge"))
    print(c1.send_message("sequence 1", "broadcast"))
    print(c1.get_message(mark_as_read=False))
    print({**c2})