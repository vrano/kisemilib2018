#-*-coding:utf-8-*-

import copy
import inspect
import json
import os

class APIHelper:
    """
        manages implemented api set
    """
    api_template = None

    def __init__(self, side):
        self.api_template = self.get_api_template(side)

    def get_api_template(self, side):
        api_path = os.path.join(
            os.path.dirname(
                os.path.abspath(__file__)
            ),
            "api_set.json"
        )
        api_template = None
        with open(api_path, "r") as f:
            api_template = json.load(f)
            api_template = {
                **api_template[side],
                **api_template["common"]
            }

        return api_template

    def check(self, message):
        """
            only provide key check and None check, not type check
        """
        if "action" not in message:
            raise KeyError("[check] action is required")
        action = message["action"]
        if action not in self.api_template:
            raise NotImplementedError("[check] unknown action {0}".format(action))
        for k in self.api_template[action]:
            if k not in message:
                raise KeyError("[check] {0} is required".format(k))
            elif message[k] is None:
                raise ValueError("[check] {0} is None. This is un-acceptable".format(k))
            else:
                pass
        
        if type(self.api_template[action]["message"]) is dict:
            for k in self.api_template[action]["message"].keys():
                if k not in message["message"]:
                    raise KeyError("[check] {0} is required".format(k))
                elif message["message"][k] is None:
                    raise ValueError("[check] {0} is None. This is un-aceptable".format(k))
                else:
                    pass

    def __getitem__(self, key):
        return copy.deepcopy(self.api_template[key])

    def __getattr__(self, property):
        return copy.deepcopy(self.api_template[property])
    
    def keys(self):
        return self.api_template.keys()

def colored(text, color, bold=False):
    c = {
        "red": "31",
        "green": "32",
        "yellow": "33",
        "blue": "34",
        "magenta": "35",
        "cyan": "36",
        "white": "37"
    }
    if bold:
        return "\033[1;{0}m{1}\033[0m".format(c[color], text)
    else:
        return "\033[{0}m{1}\033[0m".format(c[color], text)

def validate_server():
    print("server side function check...")
    api = APIHelper("server_side")
    print(
        "{0} functions should be implemented...".format(
            colored(
                len(api.keys()),
                "yellow"
            )
        )
    )

    from sup_obj import messagepool, mobileobject
    mp = messagepool.MessagePool()
    mo = mobileobject.MobileObject("testing", "testing")

    mp_members = [m[0] for m in inspect.getmembers(mp, inspect.ismethod)]
    mo_members = [m[0] for m in inspect.getmembers(mo, inspect.ismethod)]

    for k in api.keys():
        found_flag = False
        found_at = ""
        if k in mp_members:
            found_flag = True
            found_at += "MessagePool "
        if k in mo_members:
            found_flag = True
            found_at += "MobileObject"
        if found_flag:
            print("{0:<20}[{1:^6}] in {2}".format(k, colored("  OK  ", "cyan"), found_at))
        else:
            print("{0:<20}[{1:^6}]".format(k, colored("FAILED", "red", bold=True)))

def validate_client():
    print("client side function check...")
    api = APIHelper("client_side")
    print(
        "{0} functions should be implemented...".format(
            colored(
                len(api.keys()),
                "yellow"
            )
        )
    )

    import communicator
    c = communicator.CommunicationClient("testing", "testing", "0.0.0.0", 8889)
    members = [m[0] for m in inspect.getmembers(c, inspect.ismethod)]
    for k in api.keys():
        if k in members:
            print("{0:<20}[{1:^6}]".format(k, colored("  OK  ", "cyan")))
        else:
            print("{0:<20}[{1:^6}]".format(k, colored("FAILED", "red", bold=True)))

def validate_impl(side="server:client"):
    if side not in ["server", "client", "server:client"]:
        raise ValueError("Unknown side: {}".format(side))
    else:
        if "server" in side:
            validate_server()
        
        if "client" in side:
            validate_client()

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--side", type=str, default="server:client", help="server, client, or server:client")
    args = parser.parse_args()

    validate_impl(args.side)