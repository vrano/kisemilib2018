import json
import socket
import threading
import time

from IPython.display import display
import ipywidgets as widgets

def poll_message(name, host, port, interval=2, **kwargs):
    layout = widgets.Layout(
        width="100%"
    )
    status_widget = widgets.Button(
        description="Message check will run every {} seconds".format(interval),
        layout=layout,
        disabled=True
    )
    # status_widget.layout = layout
    # status_widget.disabled = True
    display(status_widget)

    def th(evt):
        request_message = {
            "control": "individual",
            "action": "get_message",
            "message": {
                "mark_as_read": False,
                "only_main": True, # this will be ignored
                "only_new": True,
                "sent_by": [] # this will be ignored
            },
            "from": name
        }

        while not evt.is_set():
            time.sleep(interval)
            try:
                connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                connection.connect((host, port))
                connection.send(
                    ( json.dumps(request_message) + "\n" ).encode()
                )

                resp = connection.recv(4096)
                resp = resp.decode()
                resp = json.loads(resp)

                connection.close()

                if resp["result"] == "fail":
                    print(resp)
                    status_widget.description = "message fetch failed"
                    status_widget.style.button_color = "#ff0000"
                else:
                    status_widget.description = "Unread message: {0}".format(
                        len(resp["message"])
                    )
                    if len(resp["message"]) != 0:
                        status_widget.style.button_color = "#ccffcc"
                    else:
                        status_widget.style.button_color="#dddddd"
            except Exception as e:
                status_widget.description = "SERVERSIDE ERROR. Please notify staff"
                status_widget.style.button_color="#ff0000"#"#fff600"
                print(e)
                return
        
        status_widget.description = "Received Stop signal. Exiting"
        status_widget.style.button_color="#ff7f00"

    stop_event = threading.Event()

    polling_thread = threading.Thread(target=th, args=(stop_event,))
    polling_thread.daemon = True
    polling_thread.start()

    return stop_event