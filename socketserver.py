#-*-coding:utf-8-*-

import argparse
import json
import logging
import socket
import threading
import time

import helper

from sup_obj import messagepool

logger = logging.getLogger(__name__)

def worker(server_socket, message_pool):
    api = helper.APIHelper("server_side")
    # worker will not die.
    #  always wait for connection
    while True:
        client_socket, (client_addr, client_port) = server_socket.accept()
        logger.info(
            "new incoming conneciton from {0}:{1} has been accepted".format(
                client_addr,
                client_port
            )
        )

        # allow treating socket as file-like object
        socket_fd = client_socket.makefile()
        client_message = socket_fd.readline()

        socket_fd.close()

        logger.debug("message: " + client_message.replace("\n", ""))

        client_message = json.loads(client_message)

        return_message = {
            "result": "fail",
            "message": "unknown"
        }

        # call functions!
        try:
            api.check(client_message)

            logger.info(
                "operation target: {0}, call: {1}".format(
                    client_message["control"],
                    client_message["action"]
                )
            )

            return_message = getattr(
                    message_pool,
                    client_message["action"]
                )(client_message)

            logger.debug("no error at requested operation: {0}".format(client_message["action"]))

        except Exception as e:
            logger.error("Exception: {0}, {1}".format(str(type(e)), e.args))
            return_message["message"] = {"error": str(type(e)), "detail": e.args}

        client_socket.send(
            (json.dumps(return_message) +"\n").encode()
        )

        client_socket.close()
        logger.info("message sent: {0}".format(json.dumps(return_message)))
        logger.info("connection with {0}:{1} has been closed".format(client_addr, client_port))

def main(host="0.0.0.0", port=8889, num_workers=4):
    # initialize thread-shared message pool object
    m = messagepool.MessagePool()

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # avoid "address already in use" by setting SO_REUSEADDR
    # socket can be re-used immediately after previous connection ends
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)

    server_socket.bind((host, port))
    server_socket.listen()

    # create and run worker threads
    threads = []
    for _ in range(num_workers):
        t = threading.Thread(target=worker, args=(server_socket, m))
        threads.append(t)
        # when parent dies, all child threads also dies.
        t.daemon = True

        t.start()

    # prevent death of main thread using while True.
    # write current message pool status eery 10 secs.
    while True:
        m.print_statistic()
        time.sleep(10)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--host", type=str, default="0.0.0.0", help="hostname/ip to bind")
    parser.add_argument("--port", "-p", type=int, default=8889, help="port to listen")
    parser.add_argument("--jobs", "-j", type=int, default=4, help="# of worker threads")
    parser.add_argument("--log", "-l", type=str, default="stdout", help="logging output: stdout or file ,or stdout:file to dual logging")
    parser.add_argument("--clear-log", "-c", type=bool, default=True, help="if specified, log file will be overwritten")
    args = parser.parse_args()

    # setting up the looger
    logger.setLevel(logging.DEBUG)

    if "stdout" in args.log:
        stdout_handler = logging.StreamHandler()
        stdout_handler.setFormatter(logging.Formatter("%(asctime)s %(name)s [%(levelname)s] %(message)s"))
        logger.addHandler(stdout_handler)
    if "file" in args.log:
        file_handler = logging.FileHandler("server.log", "w") if args.clear_log else logging.FileHandler("server.log")
        file_handler.setFormatter(logging.Formatter("%(asctime)s %(name)s [%(levelname)s] %(message)s"))
        logger.addHandler(file_handler)

    logger.debug("Server will run at {0}:{1} with {2} jobs".format(args.host, args.port, args.jobs))

    main(args.host, args.port, args.jobs)
