#-*-coding:utf-8-*-

import inspect
import threading

from . import mobileobject

class MessagePool:
    pool = None
    lock = None

    def __init__(self):
        self.pool = {}
        # assumes this pool is used in multi-thread condition, so it has lock
        self.lock = threading.Lock()

    def register_me(self, message):
        if type(message["message"]["name"]) is not str:
            raise TypeError("[{0}] name must be str".format(inspect.currentframe().f_code.co_name))
        else:
            name = message["message"]["name"]

        if type(message["message"]["category"]) is not str:
            raise TypeError("[{0}] category must be str".format(inspect.currentframe().f_code.co_name))
        else:
            category = message["message"]["category"] 

        self.lock.acquire()

        ret = {
            "result": "fail",
            "message": "unknown"
        }

        if name in self.pool:
            ret["message"] = "name already in use: {0}".format(name)
        else:
            n = mobileobject.MobileObject(name, category)
            self.pool[name] = n

            ret["result"] = "success"
            ret["message"] = "registered new name: {0} as {1}".format(name, category)

        self.lock.release()

        return ret

    def send_message(self, message):
        if type(message["to"]) is not str:
            raise TypeError("[{0}] to must be str".format(inspect.currentframe().f_code.co_name))
        else:
            to_name = message["to"]

        self.lock.acquire()

        ret = {
            "result": "fail",
            "message": "unknown"
        }

        if to_name == "broadcast":
            if len(self.pool.keys()) == 0:
                ret["message"] = "no one is registered for now"
            else:
                for k in self.pool.keys():
                    self.pool[k].register_message(message)
                ret["result"] = "success"
                ret["message"] = "message has been sent to {0}".format(to_name)
        elif to_name not in self.pool:
            ret["message"] = "{0} is not registered".format(to_name)
        else:
            self.pool[to_name].register_message(message)
            ret["result"] = "success"
            ret["message"] = "message has been sent to {0}".format(to_name)

        self.lock.release()

        return ret

    def send_status(self, message):
        if type(message["from"]) is not str:
            raise TypeError("[{0}] from must be str".format(inspect.currentframe().f_code.co_name))
        else:
            from_name = message["from"]

        self.lock.acquire()

        ret = {
            "result": "fail",
            "message": "unknown"
        }

        if from_name not in self.pool:
            ret["message"] = "status sender {0} not registered".format(from_name)
        else:
            self.pool[from_name].register_status(message["message"]["status"])
            ret["result"] = "success"
            ret["message"] = "registered new status for {0}".format(from_name)

        self.lock.release()

        return ret

    def send_position(self, message):
        if type(message["from"]) is not str:
            raise TypeError("[{0}] from must be str".format(inspect.currentframe().f_code.co_name))
        else:
            from_name = message["from"]

        self.lock.acquire()

        ret = {
            "result": "fail",
            "message": "unknown"
        }

        if from_name not in self.pool:
            ret["message"] = "status sender {0} not registered".format(from_name)
        else:
            self.pool[from_name].register_position(message["message"]["position"])
            ret["result"] = "success"
            ret["message"] = "registered new status for {0}".format(from_name)

        self.lock.release()

        return ret

    def get_message(self, message):
        if message["from"] is None:
            raise TypeError("[{0}] from must be str".format(inspect.currentframe().f_code.co_name))
        else:
            from_name = message["from"]

        if type(message["message"]["only_new"]) is not bool:
            raise TypeError("[{0}] only_new must be bool".format(inspect.currentframe().f_code.co_name))
        else:
            only_new = message["message"]["only_new"]

        if type(message["message"]["only_main"]) is not bool:
            raise TypeError("[{0}] only_main must be bool".format(inspect.currentframe().f_code.co_name))
        else:
            only_main = message["message"]["only_main"]

        if type(message["message"]["sent_by"]) is not list:
            raise TypeError("[{0}] only_new must be list".format(inspect.currentframe().f_code.co_name))
        else:
            sent_by = message["message"]["sent_by"]

        if type(message["message"]["mark_as_read"]) is not bool:
            raise TypeError("[{0}] mark_as_read must be bool".format(inspect.currentframe().f_code.co_name))
        else:
            mark_as_read = message["message"]["mark_as_read"]

        self.lock.acquire()

        ret = {
            "result": "fail",
            "message": "unknown"
        }
        if from_name not in self.pool:
            ret["message"] = "target name not registered"
        else:
            ret["result"] = "success"
            ret["message"] = self.pool[from_name].get_message(only_new, only_main, sent_by, mark_as_read)

        self.lock.release()

        return ret

    def print_statistic(self):
        """
            print current message pool status.
        """
        self.lock.acquire()

        print("           |           | {0:^21} |          |".format("message received"))
        print("{0:>10} | {1:>9} | {2:^21} | {3:>8} | {4}".format("name", "category", "before/after delivery", "position", "status"))

        for k in sorted(self.pool.keys()):
            report = self.pool[k].get_statistic()
            print(
                "{0:>10} | {1:>9} | {2:^21} | {3:>8} | {4}".format(
                    report["name"],
                    report["category"],
                    "{0}/{1}".format(
                        report["message"]["before delivery"],
                        report["message"]["after delivery"]
                    ),
                    report["status"]["position"],
                    report["status"]["current action"]
                )
            )
        else:
            print("---------- | --------- | --------------------- | -------- | ------")
            print("")

        self.lock.release()

    def get_latest_status(self, message):
        """
            get latest position/status of the obejcts in message pool.
            this is for web visualization.
        """
        if type(message["message"]["acquire_user"]) is not list:
            raise TypeError("[{0}] acquire_user must be list".format(inspect.currentframe().f_code.co_name))
        else:
            acquire_user = message["message"]["acquire_user"]

        self.lock.acquire()

        ret = {
            "result": "fail",
            "message": "unknown"
        }

        # 全員
        result = {}
        target_keys = sorted(self.pool.keys()) if len(acquire_user) == 0 else sorted(acquire_user)
        for k in target_keys:
            r = {"status": None, "position": None, "category": None}

            r["category"] = self.pool[k].category
            r["status"] = self.pool[k].get_status()
            r["position"] = self.pool[k].get_position()

            result[k] = r
        
        ret["result"] = "success"
        ret["message"] = result

        self.lock.release()

        return ret