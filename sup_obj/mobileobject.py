#-*-coding:utf-8-*-

import time

class MobileObject:
    """
        this class represents the objects like taxi, customer...
    """
    message_sent = None
    message_received = None
    position = None
    status = None
    name = None
    category = None

    def __init__(self, name, category):
        self.message_received = []
        self.position = {"time": time.time(), "original_position": -1}
        self.status = {"time": time.time(), "original_status": "No action"}
        self.name = name
        self.category = category

    def register_message(self, message):
        """
            register a message which is sent to me from others
        """
        append_message = {
            "already_delivered": False,
            "time": time.time(),
            "original_message": message
        }
        self.message_received.append(append_message)

    def register_status(self, status):
        """
            register a status of me
        """
        append_status = {
            "time": time.time(),
            "original_status": status
        }

        self.status = append_status

    def register_position(self, position):
        append_position = {
            "time": time.time(),
            "original_position": position
        }

        self.position = append_position

    def get_message(self, only_new, only_main, sent_by, mark_as_read):
        ret = []

        if only_new:
            ret = [m["original_message"] for m in self.message_received if not m["already_delivered"]]
        else:
            ret = [m["original_message"] for m in self.message_received]

        if mark_as_read:
            for m in self.message_received:
                m["already_delivered"] = True

        if sent_by:
            ret = [m for m in ret if m["from"] in sent_by]

        if only_main:
            ret = [
                {"message": r["message"], "from": r["from"]} for r in ret]

        return ret

    def get_status(self):
        return self.status["original_status"]

    def get_position(self):
        return self.position["original_position"]

    def get_statistic(self):
        ret = {
            "name": self.name,
            "category": self.category,
            "message": {
                "before delivery": len([m for m in self.message_received if not m["already_delivered"]]),
                "after delivery": len([m for m in self.message_received if m["already_delivered"]])
            },
            "status": {
                "position": self.position["original_position"],
                "current action": self.status["original_status"]
            }
        }

        return ret