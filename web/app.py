#-*-coding:utf-8-*-

import argparse
import json
import logging
import os
import socket
import sys
import time

from datetime import datetime, timedelta

from tornado import autoreload, ioloop, web, websocket

logger = logging.getLogger(__name__)

server_location = "localhost:8899"

socket_location = {
    "ip": "0.0.0.0",
    "port": 8889
}

class MainHandler(web.RequestHandler):
    def get(self):
        self.render(
            "index.html",
            server_location=server_location
        )

    def data_received(self, chunk):
        pass

class StatusHandler(websocket.WebSocketHandler):
    def check_origin(self, origin):
        return True

    def open(self):
        ioloop.IOLoop.current().add_timeout(
            time.time() + 1,
            self.check_status
        )
    
    def on_close(self):
        pass
    
    def data_received(self):
        pass
    
    def check_status(self):
        ret = {}
        try:
            connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            connection.connect((socket_location["ip"], socket_location["port"]))
            connection.send((json.dumps(
                {
                    "control": "pool",
                    "action": "get_latest_status",
                    "message": {
                        "acquire_user": []
                    },
                    "from": "web-vis"
                }
            ) + "\n").encode())

            # may need change
            resp = connection.recv(4096)
            resp = resp.decode()
            # print(resp)

            connection.close()

            ret = json.loads(resp)
        except ConnectionRefusedError:
            ret = {"message":{"error":[]}}

        payload = {}
        for k in ret["message"].keys():
            if len(ret["message"][k]) == 0:
                payload[k] = {"position": "not registered", "status": "not registered"}
            else:
                payload[k] = ret["message"][k]

        self.write_message(json.dumps(payload))

        ioloop.IOLoop.current().add_timeout(
            time.time() + 1,
            self.check_status

        )

application = web.Application(
    [
        (r"/", MainHandler),
        (r"/status", StatusHandler)
    ],
    template_path=os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        "templates"
    ),
    static_path=os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        "static"
    ),
    debug=True
)

def main(host, port):
    application.listen(port)
    ioloop.IOLoop.current().start()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--host", type=str, default="0.0.0.0", help="hostname/ip to bind")
    parser.add_argument("--port", "-p", type=int, default=8899, help="port to listen")
    parser.add_argument("--log", "-l", type=str, default="stdout", help="logging output: stdout or file ,or stdout:file to dual logging")
    parser.add_argument("--clear-log", "-c", type=bool, default=True, help="if specified, log file will be overwritten")
    args = parser.parse_args()

    logger.setLevel(logging.DEBUG)

    if "stdout" in args.log:
        stdout_handler = logging.StreamHandler()
        stdout_handler.setFormatter(logging.Formatter("%(asctime)s %(name)s [%(levelname)s] %(message)s"))
        logger.addHandler(stdout_handler)
    if "file" in args.log:
        file_handler = logging.FileHandler("app.log", "w") if args.clear_log else logging.FileHandler("app.log")
        file_handler.setFormatter(logging.Formatter("%(asctime)s %(name)s [%(levelname)s] %(message)s"))
        logger.addHandler(file_handler)


    logger.debug("Server will run at {0}:{1}".format(args.host, args.port))

    server_location = "{0}:{1}".format(args.host, args.port)

    main(args.host, args.port)