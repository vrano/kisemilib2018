var divs = {};

var socket = new WebSocket('ws://' + server_location + '/status');
socket.onopen = function(event){
    console.log("connected");
}
socket.onclose = function(){
    console.log("disconnected");
}
socket.onmessage = function(event){
    var status = JSON.parse(event.data);
    update_view(status);
}

function update_view(status){
    for (user in status){
        if (!(user in divs)){
            var user_wrapper = document.createElement("div");
            user_wrapper.className = "user_wrapper";
            user_wrapper.id = user;

            var user_name = document.createElement("div");
            user_name.className = "user_name";
            user_name.innerText = user;
            user_wrapper.appendChild(user_name);

            var user_category = document.createElement("div");
            user_category.className = "user_category";
            user_wrapper.appendChild(user_category);

            var user_position = document.createElement("div");
            user_position.className = "user_position";
            user_wrapper.appendChild(user_position);

            var user_status = document.createElement("div");
            user_status.className = "user_status";
            user_wrapper.appendChild(user_status);

            var status_parent = document.getElementById("wrapper");
            status_parent.appendChild(user_wrapper);

            divs[user] = user_wrapper;
        }

        var target = document.getElementById(user);
        target.getElementsByClassName("user_category")[0].innerText = status[user].category;
        target.getElementsByClassName("user_position")[0].innerText = status[user].position;
        target.getElementsByClassName("user_status")[0].innerText = status[user].status;
        
    }
}